import React from "react";
import {reactRender} from "@ombiel/aek-lib";

import Screen from "./scholarship-finder/screen";

reactRender(<Screen />);
