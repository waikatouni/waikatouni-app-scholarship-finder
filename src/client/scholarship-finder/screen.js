import React, 
{ 
  useState 
} from "react";

import { Line } from 'rc-progress';
import { Button, Input } from "@ombiel/aek-lib";
import { Wizard, Steps, Step } from 'react-albus';

import {
  Container,
} from "@ombiel/aek-lib";

const Screen = (props) => {
  const [search, setSearch] = useState("");
  const [progress, setProgress] = useState("0")
  const [hideButton, setHideButton] = useState(false)
  const [color, setColor] = useState("#D9D9D9");
  const [searchString, setSearchString] = useState("");
  const [stepZeroOption, setStepZeroOption] = useState("Open,Closed");
  const [stepOneOption, setStepOneOption] = useState("");
  const [stepTwoOption, setStepTwoOption] = useState("");
  const [stepThreeOption, setStepThreeOption] = useState("");
  const [stepFourOption, setStepFourOption] = useState("");

  const _changeStepOneCheckbox = (value) => {
      if(value.target.value === stepOneOption) {
        setStepOneOption("");
      } else {
        setStepOneOption(value.target.value);
      }
  }

  const _changeStepTwoCheckbox = (value) => {
    if(value.target.value === stepTwoOption) {
      setStepTwoOption("");
    } else {
      setStepTwoOption(value.target.value);
    }
  }

  const _changeStepThreeCheckbox = (value) => {
    if(value.target.value === stepThreeOption) {
      setStepThreeOption("");
    } else {
      setStepThreeOption(value.target.value);
    }
  }

  const _changeStepFourCheckbox = (value) => {
    if(value.target.value === stepFourOption) {
      setStepFourOption("");
    } else {
      setStepFourOption(value.target.value);
    }
  }


  const _serachForScholarship = (value) => {
    const searchValue = value.target.value;
    const searchableString = searchValue.replace(' ', '%20');
    const serchURL = "https://www.waikato.ac.nz/scholarships/search#!/?page=1&q=" + searchableString;

    setSearchString(searchValue);
    setSearch(serchURL);

    if(value.target.value == "") {
      setSearchString("");
      setSearch("");
    }
  }

  const _clearSearch = () => {
    setSearchString("");
    setSearch("");
  }

  return(
    <Container>
      <div style={{ display: "flex", width: "100%", flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
      <div style={{ width: "100%", maxWidth: 450 }}>
        <Line percent={progress} strokeWidth="2" strokeColor={color} style={{ display: "flex", margin: 20, alignSelf: "center" }} />
      </div>
      <Wizard>
        <Steps>
          <Step
            id="stepOne"
            render={({ next }) => (
              <div style={{ display: "flex", flexDirection: "column", width: "100%", height: "100%", alignItems: "center", justifyContent: "center", paddingLeft: 15, paddingRight: 15, paddingBottom: 100  }}>
                <h1 style={{ textAlign: "center" }}>Scholarship Search Options</h1>
                <p style={{ maxWidth: 450, textAlign: 'justify' }}>The University of Waikato and our partners offer a range of scholarships and awards to assist students with fees and living expenses, and to support academic and research excellence. To search our database to find a scholarship or award that is right for you, use the filters on this page.</p>
                <p style={{ maxWidth: 450, textAlign: 'justify' }}>In addition to the scholarships listed on our Scholarships Finder there are also other <a href='https://www.waikato.ac.nz/scholarships/update'>external opportunities</a> that may be of interest.</p>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", marginBottom: 15, marginTop: 15, width: "100%", maxWidth: 350, height: 40 }}>
                  <Input icon="search" name="name" placeholder="Search for scholarships..." fluid size="large" onFocus={() => setHideButton(true)} onBlur={() => setHideButton(false)} onChange={_serachForScholarship} value={searchString}>
                    <input/>
                    <Button disabled={!search} link={search} target="_blank" onClick={_clearSearch} style={{ backgroundColor: "#d40100", color: "white", fontWeight: "bold", letterSpacing: 1 }}>Search</Button> 
                  </Input>
                </div>
                <h2>Student Type</h2>
                <div style={{ display: "flex", flexDirection: "column", justifyContent: "flex-start", backgroundColor: "whitesmoke", padding: 30, borderRadius: 10 }}>
                  <h3>Are you:</h3>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                  <input onChange={_changeStepOneCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="future" name="future" value="future" checked={stepOneOption === 'future'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>New to Waikato</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex",paddingBottom: 15 }}>
                  <input onChange={_changeStepOneCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="current" name="current" value="current" checked={stepOneOption === 'current'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>A current / past student of Waikato</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 0 }}>
                  <input onChange={_changeStepOneCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id=" " name=" " value=" " checked={stepOneOption === ' '} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Show both</label>
                </div>
                </div>
                {!hideButton && <div style={{ width: "100%", backgroundColor: 'white', display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "flex-end", position: "fixed", bottom: 0, padding: 10, paddingTop: 15, paddingBottom: 15, boxShadow: '1px -8px 34px -8px rgba(0,0,0,0.39)' }}>
                  <Button style={{ width: "45%", borderRadius: 10, padding: 10, color: "white", backgroundColor: "#d40100", borderWidth: 0, letterSpacing: 1, fontWeight: "bold", fontSize: 16, }} onClick={() => { next(); setColor("#FF6400"); setProgress("25"); }}>Next</Button>
                </div>}
              </div>
            )}
          />
          <Step
            id="stepTwo"
            render={({ next, previous }) => (
              <div style={{ width: "100%", alignItems: "center", justifyContent: "center", display: "flex", flexDirection: "column", paddingLeft: 15, paddingRight: 15, paddingBottom: 150   }}>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", marginBottom: 15, width: "100%", maxWidth: 350, height: 40 }}>
                  <Input icon="search" name="name" placeholder="Search for scholarships..." fluid size="large" onFocus={() => setHideButton(true)} onBlur={() => setHideButton(false)} onChange={_serachForScholarship} value={searchString} >
                    <input/>
                    <Button disabled={!search} link={search} target="_blank" onClick={_clearSearch} style={{ backgroundColor: "#d40100", color: "white", fontWeight: "bold", letterSpacing: 1 }}>Search</Button> 
                  </Input>
                </div>
                <h2>Enrolment Status</h2>
                <div style={{ display: "flex", flexDirection: "column", justifyContent: "flex-start", backgroundColor: "whitesmoke", padding: 30, borderRadius: 10 }}>
                <h3>Do you want to study:</h3>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                  <input onChange={_changeStepTwoCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="fulltime" name="fulltime" value="fulltime" checked={stepTwoOption === 'fulltime'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Full Time</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                  <input onChange={_changeStepTwoCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="parttime" name="parttime" value="parttime" checked={stepTwoOption === 'parttime'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Part Time</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 0 }}>
                  <input onChange={_changeStepTwoCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id=" " name=" " value=" " checked={stepTwoOption === ' '} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Show both</label>
                </div>
                </div>
                {!hideButton && <div style={{ width: "100%", backgroundColor: 'white', display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-between", position: "fixed", bottom: 0, padding: 10, paddingTop: 15, paddingBottom: 15, boxShadow: '1px -8px 34px -8px rgba(0,0,0,0.39)' }}>
                  <Button style={{ width: "45%", borderRadius: 10, padding: 10, color: "white", backgroundColor: "#7d7d7d", borderWidth: 0, letterSpacing: 1, fontWeight: "bold", fontSize: 16 }} onClick={() => {previous(); setColor("#D9D9D9"); setProgress("0");}}>Previous</Button>
                  <Button style={{ width: "45%", borderRadius: 10, padding: 10, color: "white", backgroundColor: "#d40100", borderWidth: 0, letterSpacing: 1, fontWeight: "bold", fontSize: 16 }} onClick={() => {next(); setColor("#FFCD00"); setProgress("65");}}>Next</Button>
                </div>}
              </div>
            )}
          />
          <Step
            id="stepThree"
            render={({ previous, next }) => (
              <div style={{ width: "100%", alignItems: "center", justifyContent: "center", display: "flex", flexDirection: "column", paddingLeft: 15, paddingRight: 15, paddingBottom: 150  }}>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", marginBottom: 15, width: "100%", maxWidth: 350, height: 40 }}>
                  <Input icon="search" name="name" placeholder="Search for scholarships..." fluid size="large" onFocus={() => setHideButton(true)} onBlur={() => setHideButton(false)} onChange={_serachForScholarship} value={searchString} >
                    <input/>
                    <Button disabled={!search} link={search} target="_blank" onClick={_clearSearch} style={{ backgroundColor: "#d40100", color: "white", fontWeight: "bold", letterSpacing: 1 }}>Search</Button> 
                  </Input>
                </div>
                <h2>Level</h2>
                <div style={{ display: "flex", flexDirection: "column", justifyContent: "flex-start", backgroundColor: "whitesmoke", padding: 30, borderRadius: 10 }}>
                  <h3>What type of scholarship do you wish to apply for?</h3>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                  <input onChange={_changeStepThreeCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="entrance" name="entrance" value="entrance" checked={stepThreeOption === 'entrance'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Entrance/School Leaver</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                  <input onChange={_changeStepThreeCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="undergrad" name="undergrad" value="undergrad" checked={stepThreeOption === 'undergrad'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Undergraduate</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                  <input onChange={_changeStepThreeCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="pgtaught" name="pgtaught" value="pgtaught" checked={stepThreeOption === 'pgtaught'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Postgraduate (Taught)</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                  <input onChange={_changeStepThreeCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="pgresearch" name="pgresearch" value="pgresearch" checked={stepThreeOption === 'pgresearch'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Postgraduate (Research)</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                  <input onChange={_changeStepThreeCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="other" name="other" value="other" checked={stepThreeOption === 'other'} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Other</label>
                </div>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 0 }}>
                  <input onChange={_changeStepThreeCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id=" " name=" " value=" " checked={stepThreeOption === ' '} />
                  <label style={{ paddingLeft: 10, fontSize: 17 }}>Show all</label>
                </div>
                </div>
                {!hideButton && <div style={{ width: "100%", backgroundColor: 'white', display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-between", position: "fixed", bottom: 0, padding: 10, paddingTop: 15, paddingBottom: 15, boxShadow: '1px -8px 34px -8px rgba(0,0,0,0.39)' }}>
                  <Button style={{ width: "45%", borderRadius: 10, padding: 10, color: "white", backgroundColor: "#7d7d7d", borderWidth: 0, letterSpacing: 1, fontWeight: "bold", fontSize: 16 }} onClick={() => {previous(); setColor("#FF6400"); setProgress("35");}}>Previous</Button>
                  <Button style={{ width: "45%", borderRadius: 10, padding: 10, color: "white", backgroundColor: "#d40100", borderWidth: 0, letterSpacing: 1, fontWeight: "bold", fontSize: 16 }} onClick={() => {next(); setColor("#06d6a0"); setProgress("100");}}>Next</Button>
                  </div>}
              </div>
            )}
          />
          <Step 
            id="stepFour"
            render={({ previous, next }) => (
              <div style={{ width: "100%", alignItems: "center", justifyContent: "center", display: "flex", flexDirection: "column", paddingLeft: 15, paddingRight: 15, paddingBottom: 150  }}>
                <div style={{ flexDirection: "row", alignItems: "center", display: "flex", marginBottom: 15, width: "100%", maxWidth: 350, height: 40 }}>
                  <Input icon="search" name="name" placeholder="Search for scholarships..." fluid size="large" onFocus={() => setHideButton(true)} onBlur={() => setHideButton(false)} onChange={_serachForScholarship} value={searchString} >
                    <input/>
                    <Button disabled={!search} link={search} target="_blank" onClick={_clearSearch} style={{ backgroundColor: "#d40100", color: "white", fontWeight: "bold", letterSpacing: 1 }}>Search</Button> 
                  </Input>
                </div>
              <h2>Subject Area</h2>
              <div style={{ display: "flex", flexDirection: "column", justifyContent: "flex-start", backgroundColor: "whitesmoke", padding: 30, borderRadius: 10 }}>
              <h3>What subject area are you going to be studying?</h3>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="arts" name="arts" value="arts" checked={stepFourOption === 'arts'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Arts & Social Sciences</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="cms" name="cms" value="cms" checked={stepFourOption === 'cms'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Computer Science/Mathematics</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="edu" name="edu" value="edu" checked={stepFourOption === 'edu'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Education</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="hshp" name="hshp" value="hshp" checked={stepFourOption === 'hshp'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Health, Sport & Human Performance</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="eng" name="eng" value="eng" checked={stepFourOption === 'eng'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Engineering</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="law" name="law" value="law" checked={stepFourOption === 'law'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Law</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="mngt" name="mngt" value="mngt" checked={stepFourOption === 'mngt'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Management/Commerce</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="mpd" name="mpd" value="mpd" checked={stepFourOption === 'mpd'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Māori & Indigenous Studies</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 15 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id="sci" name="sci" value="sci" checked={stepFourOption === 'sci'} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Science</label>
              </div>
              <div style={{ flexDirection: "row", alignItems: "center", display: "flex", paddingBottom: 0 }}>
                <input onChange={_changeStepFourCheckbox} style={{ width: 20, height: 20 }} type="checkbox" id=" " name=" " value=" " checked={stepFourOption === ' '} />
                <label style={{ paddingLeft: 10, fontSize: 17 }}>Show all</label>
              </div>
              </div>
              {!hideButton && <div style={{ width: "100%", backgroundColor: 'white', display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-between", position: "fixed", bottom: 0, padding: 10, paddingTop: 15, paddingBottom: 15, boxShadow: '1px -8px 34px -8px rgba(0,0,0,0.39)' }}>
                <Button style={{ width: "45%", borderRadius: 10, padding: 10, color: "white", backgroundColor: "#7d7d7d", borderWidth: 0, letterSpacing: 1, fontWeight: "bold", fontSize: 16 }} onClick={() => {previous(); setColor("#fff600"); setProgress("65");}}>Previous</Button>
                <Button 
                  link={"https://www.waikato.ac.nz/scholarships/search#!/?page=1" 
                          + (stepZeroOption && ("&Applications=" + stepZeroOption))
                          + (stepOneOption && stepOneOption !== ' ' ? ("&Type=" + stepOneOption) : '')
                          + (stepTwoOption && stepTwoOption !== ' ' ? ("&Status=" + stepTwoOption) : '')
                          + (stepThreeOption && stepThreeOption !== ' ' ? ("&Level=" + stepThreeOption) : '')
                          + (stepFourOption &&  stepFourOption !== ' ' ? ("&Subject=" + stepFourOption) : '')}
                  style={{ width: "45%", borderRadius: 10, padding: 10, color: "white", backgroundColor: "#d40100", borderWidth: 0, letterSpacing: 1, fontWeight: "bold", fontSize: 16 }}>Finish</Button>
              </div>}
            </div>
            )}
          />
        </Steps>
      </Wizard>
      </div>
    </Container>
  );
}

export default Screen;